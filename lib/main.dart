import 'package:dating_app/screens/LoginScreen.dart';
import 'package:dating_app/screens/ProfileScreen.dart';
import 'package:dating_app/screens/RegistrationScreen.dart';
import 'package:dating_app/screens/TestScreen.dart';
import 'package:flutter/material.dart';
import 'package:dating_app/screens/HomeScreen.dart';
import 'package:dating_app/models/TestData.dart';
import 'package:dating_app/Globals.dart' as globals;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/': (BuildContext context) => LoginScreen(),
        '/registration': (BuildContext context) => RegistrationScreen(),
        '/profile': (BuildContext context) => ProfileScreen(),
        '/profile/test': (BuildContext context) => TestScreen(
          globals.userProfile.sex == 'm'
              ? Sex.male
              : Sex.female,
          globals.userId,
        ),
        '/home': (BuildContext context) => HomeScreen(),
        '/home/profile': (BuildContext context) => ProfileScreen(profile: globals.userProfile),
        '/home/profile/test': (BuildContext context) => TestScreen(
          globals.userProfile.sex == 'm'
              ? Sex.male
              : Sex.female,
          globals.userId,
          answers: globals.userProfile.answers,
        ),
      },
      debugShowCheckedModeBanner: false,
      title: 'Tindor',
      theme: ThemeData(
        primaryColor: Colors.white,
        accentColor: Colors.blue,
        unselectedWidgetColor: Colors.black,
      ),
    );
  }
}