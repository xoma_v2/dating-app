import 'dart:async';
import 'dart:convert';
import 'dart:core' as prefix0;
import 'dart:core';
import 'dart:io';
import 'package:dating_app/Globals.dart' as globals;
import 'package:dating_app/models/ValueController.dart';
import 'package:flutter/material.dart';
import 'package:http/io_client.dart';
import 'models/FullProfile.dart';
import 'package:dating_app/WebAPI.dart' as webAPI;

Future<int> authUser(String login, String password) async {
  bool trustSelfSigned = true;
  HttpClient httpClient = new HttpClient()
    ..badCertificateCallback = ((X509Certificate cert, String host, int port) => trustSelfSigned);

  String url = '${globals.domain}api/account';
  var body = {
    "Login": login,
    "Password": password,
  };
  var headers = {
    "Content-type": "application/json",
  };

  IOClient ioClient = new IOClient(httpClient);
  var response = await ioClient.post(Uri.parse(url), body: json.encode(body), headers: headers);
  ioClient.close();

  if (response.statusCode == 200) {
    var decodedResponse = json.decode(response.body);
    globals.token = decodedResponse["access_token"];
    int userId = int.parse(decodedResponse["user_id"]);
    globals.userProfile = FullProfile(userId: userId);

    globals.closestController= ValueFetchController(ValueController(userId, (id) => webAPI.getClosest(id)));
    globals.marriageController = ValueFetchController(ValueController(userId, (id) => webAPI.getMarriage(id)));
    globals.favoriteController = ValueFetchController(ValueController(userId, (id) => webAPI.getFavorites(id)));
  } else {
    globals.token = null;
    globals.userProfile = null;
  }

  return response.statusCode;
}

Future<FullProfile> getFullProfile(int targetId, {int senderId}) async {
  bool trustSelfSigned = true;
  HttpClient httpClient = new HttpClient()
    ..badCertificateCallback = ((X509Certificate cert, String host, int port) => trustSelfSigned);

  String url = senderId == null
    ? '${globals.domain}api/datingapp/fullprofile/$targetId'
    : '${globals.domain}api/datingapp/fullprofile/$targetId/$senderId';
  var headers = {
    "Authorization": globals.auth,
  };

  IOClient ioClient = new IOClient(httpClient);
  var response = await ioClient.get(Uri.parse(url), headers: headers);
  ioClient.close();

  if (response.body.isEmpty) {
    return null;
  } else {
    var decodedResponse = json.decode(response.body);
    return FullProfile.fromJson(decodedResponse);
  }
}

Future<void> putFullProfile(FullProfile profile) async {
  bool trustSelfSigned = true;
  HttpClient httpClient = new HttpClient()
    ..badCertificateCallback = ((X509Certificate cert, String host, int port) => trustSelfSigned);

  String url = '${globals.domain}api/datingapp/fullprofile';
  var body = profile.toJson();
  var headers = {
    "Content-type": "application/json",
    "Authorization": globals.auth,
  };

  print(json.encode(body));

  IOClient ioClient = new IOClient(httpClient);
  await ioClient.put(Uri.parse(url), body: json.encode(body), headers: headers);
  ioClient.close();
}

Future<List<FullProfile>> getFullProfiles(List<int> targetIds, int senderId) async {
  bool trustSelfSigned = true;
  HttpClient httpClient = new HttpClient()
    ..badCertificateCallback = ((X509Certificate cert, String host, int port) => trustSelfSigned);

  String url = '${globals.domain}api/datingapp/fullprofiles/$senderId';
  var body = targetIds;
  var headers = {
    "Content-type": "application/json",
    "Authorization": globals.auth,
  };

  IOClient ioClient = new IOClient(httpClient);
  var response = await ioClient.post(Uri.parse(url), body: json.encode(body), headers: headers);
  ioClient.close();
  var decodedResponse = json.decode(response.body);
  
  return FullProfile.fromJsonList(decodedResponse);
}

Future<List<FullProfile>> getFullProfilesWithCache(List<int> targetIds, int senderId, Map<int, FullProfile> cache) async {
  List<FullProfile> result = new List(targetIds.length);
  for (int i = 0, j = 0; i < targetIds.length; i++) {
    if (cache[targetIds[j]] != null) {
      result[i] = cache[targetIds[j]];
      targetIds.remove(targetIds[j]);
    } else {
      result[i] = null;
      j++;
    }
  }

  List<FullProfile> profiles = await getFullProfiles(targetIds, senderId);
  for (int i = 0, j = 0; i < result.length; i++) {
    if (result[i] == null) {
      result[i] = profiles[j];
      cache.putIfAbsent(profiles[j].userId, () => profiles[j]);
      j++;
    }
  }

  return result;
}

Future<FullProfile> getProfile(int targetId) async {
  bool trustSelfSigned = true;
  HttpClient httpClient = new HttpClient()
    ..badCertificateCallback = ((X509Certificate cert, String host, int port) => trustSelfSigned);

  String url = '${globals.domain}api/datingapp/profile/$targetId';
  var headers = {
    "Authorization": globals.auth,
  };

  IOClient ioClient = new IOClient(httpClient);
  var response = await ioClient.get(Uri.parse(url), headers: headers);
  ioClient.close();
  var decodedResponse = json.decode(response.body);
  
  return FullProfile.fromJson(decodedResponse);
}

Future<void> putFavorite(int targetId, int senderId) async {
  bool trustSelfSigned = true;
  HttpClient httpClient = new HttpClient()
    ..badCertificateCallback = ((X509Certificate cert, String host, int port) => trustSelfSigned);

  String url = '${globals.domain}api/datingapp/favorite/$targetId/$senderId';
  var headers = {
    "Authorization": globals.auth,
  };

  IOClient ioClient = new IOClient(httpClient);
  await ioClient.put(Uri.parse(url), headers: headers);
  ioClient.close();
}

Future<void> deleteFavorite(int targetId, int senderId) async {
  bool trustSelfSigned = true;
  HttpClient httpClient = new HttpClient()
    ..badCertificateCallback = ((X509Certificate cert, String host, int port) => trustSelfSigned);

  String url = '${globals.domain}api/datingapp/favorite/$targetId/$senderId';
  var headers = {
    "Authorization": globals.auth,
  };

  IOClient ioClient = new IOClient(httpClient);
  await ioClient.delete(Uri.parse(url), headers: headers);
  ioClient.close();
}

Future<List<int>> getClosest(int id, {StreamController<List<int>> controller}) async {
  bool trustSelfSigned = true;
  HttpClient httpClient = new HttpClient()
    ..badCertificateCallback = ((X509Certificate cert, String host, int port) => trustSelfSigned);

  String url = '${globals.domain}api/datingapp/closest/$id';
  var headers = {
    "Authorization": globals.auth,
  };

  IOClient ioClient = new IOClient(httpClient);
  var response = await ioClient.get(Uri.parse(url), headers: headers);
  ioClient.close();

  List<String> decodedResponse = response.body.replaceAll('[', '').replaceAll(']', '').split(',');
  List<int> userIds = [];
  for (var id in decodedResponse) {
    userIds.add(int.parse(id));
  }

  if (controller != null) {
    controller.sink.add(userIds);
  }
  return userIds;
}

Future<List<int>> getMarriage(int id, {StreamController<List<int>> controller}) async {
  bool trustSelfSigned = true;
  HttpClient httpClient = new HttpClient()
    ..badCertificateCallback = ((X509Certificate cert, String host, int port) => trustSelfSigned);

  String url = '${globals.domain}api/datingapp/marriage/$id';
  var headers = {
    "Authorization": globals.auth,
  };

  IOClient ioClient = new IOClient(httpClient);
  var response = await ioClient.get(Uri.parse(url), headers: headers);
  ioClient.close();

  List<String> decodedResponse = response.body.replaceAll('[', '').replaceAll(']', '').split(',');
  List<int> userIds = [];
  for (var id in decodedResponse) {
    userIds.add(int.parse(id));
  }

  if (controller != null) {
    controller.sink.add(userIds);
  }
  return userIds;
}

Future<List<int>> getAnswers(int id) async {
  bool trustSelfSigned = true;
  HttpClient httpClient = new HttpClient()
    ..badCertificateCallback = ((X509Certificate cert, String host, int port) => trustSelfSigned);

  String url = '${globals.domain}api/datingapp/answers/$id';
  var headers = {
    "Authorization": globals.auth,
  };

  IOClient ioClient = new IOClient(httpClient);
  var response = await ioClient.get(Uri.parse(url), headers: headers);
  ioClient.close();

  if (response.body.isEmpty) {
    return null;
  } else {
    List<String> decodedResponse = response.body.replaceAll('[', '').replaceAll(']', '').split(',');
    List<int> answers = [];
    for (var answer in decodedResponse) {
      answers.add(int.parse(answer));
    }

    return answers;
  }
}

Future<void> putAnswers(List<int> answers, int id) async {
  bool trustSelfSigned = true;
  HttpClient httpClient = new HttpClient()
    ..badCertificateCallback = ((X509Certificate cert, String host, int port) => trustSelfSigned);

  String url = '${globals.domain}api/datingapp/answers/$id';
  var body = answers;
  var headers = {
    "Content-type": "application/json",
    "Authorization": globals.auth,
  };

  IOClient ioClient = new IOClient(httpClient);
  await ioClient.put(Uri.parse(url), body: json.encode(body), headers: headers);
  ioClient.close();
}

Future<void> postAnswers(List<int> answers, int id) async {
  bool trustSelfSigned = true;
  HttpClient httpClient = new HttpClient()
    ..badCertificateCallback = ((X509Certificate cert, String host, int port) => trustSelfSigned);

  String url = '${globals.domain}api/datingapp/answers/$id';
  var body = answers;
  var headers = {
    "Content-type": "application/json",
    "Authorization": globals.auth,
  };

  IOClient ioClient = new IOClient(httpClient);
  await ioClient.post(Uri.parse(url), body: json.encode(body), headers: headers);
  ioClient.close();
}

Future<List<FullProfile>> getFavoriteProfiles(int id) async {
  bool trustSelfSigned = true;
  HttpClient httpClient = new HttpClient()
    ..badCertificateCallback = ((X509Certificate cert, String host, int port) => trustSelfSigned);

  String url = '${globals.domain}api/datingapp/favoriteprofiles/$id';
  var headers = {
    "Authorization": globals.auth,
  };

  IOClient ioClient = new IOClient(httpClient);
  var response = await ioClient.get(Uri.parse(url), headers: headers);
  ioClient.close();
  var decodedResponse = json.decode(response.body);
  
  return FullProfile.fromJsonList(decodedResponse);
}

Future<List<FullProfile>> getFavoriteProfilesFromTo(int id, int indexFrom, int indexTo) async {
  bool trustSelfSigned = true;
  HttpClient httpClient = new HttpClient()
    ..badCertificateCallback = ((X509Certificate cert, String host, int port) => trustSelfSigned);

  String url = '${globals.domain}api/datingapp/favoriteprofiles/$id/$indexFrom/$indexTo';
  var headers = {
    "Authorization": globals.auth,
  };

  IOClient ioClient = new IOClient(httpClient);
  var response = await ioClient.get(Uri.parse(url), headers: headers);
  ioClient.close();
  var decodedResponse = json.decode(response.body);
  
  print (FullProfile.fromJsonList(decodedResponse));
  return FullProfile.fromJsonList(decodedResponse);
}

Future<List<int>> getFavorites(int id) async {
  bool trustSelfSigned = true;
  HttpClient httpClient = new HttpClient()
    ..badCertificateCallback = ((X509Certificate cert, String host, int port) => trustSelfSigned);

  String url = '${globals.domain}api/datingapp/favorites/$id';
  var headers = {
    "Authorization": globals.auth,
  };

  IOClient ioClient = new IOClient(httpClient);
  var response = await ioClient.get(Uri.parse(url), headers: headers);
  ioClient.close();

  if (response.body == '[]') {
    return [];
  } else {
    List<String> decodedResponse = response.body.replaceAll('[', '').replaceAll(']', '').split(',');
    List<int> userIds = [];
    
    for (var id in decodedResponse) {
      userIds.add(int.parse(id));
    }
    
    return userIds;
  }
}

Future<bool> checkLogin(String login) async {
  bool trustSelfSigned = true;
  HttpClient httpClient = new HttpClient()
    ..badCertificateCallback = ((X509Certificate cert, String host, int port) => trustSelfSigned);

  String url = '${globals.domain}api/account/$login';

  IOClient ioClient = new IOClient(httpClient);
  var response = await ioClient.get(Uri.parse(url));
  ioClient.close();

  String decodedResponse = response.body;

  return decodedResponse == 'true'
      ? true
      : false;
}

Future<bool> putUser(String login, String password) async {
  bool trustSelfSigned = true;
  HttpClient httpClient = new HttpClient()
    ..badCertificateCallback = ((X509Certificate cert, String host, int port) => trustSelfSigned);

  String url = '${globals.domain}api/account';
  var body = {
    "Login": login,
    "Password": password,
  };
  var headers = {
    "Content-type": "application/json",
  };

  IOClient ioClient = new IOClient(httpClient);
  var response = await ioClient.put(Uri.parse(url), body: json.encode(body), headers: headers);
  ioClient.close();
  
  bool decodedResponse = json.decode(response.body);
  return decodedResponse;
}