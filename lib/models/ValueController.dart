import 'dart:async';

class ValueFetchController<T, P>{
  ValueController<T, P> _controller;

  ValueFetchController(this._controller);

  void onReady(void Function(T) func) {
    if (_controller.valueDone) {
      func(_controller.value);
    } else {
      _controller.subscribe(func);
      _controller.start();
    }
  }

  void restart({P param}) {
    _controller.restart(param: param);
  }
}

class ValueController<T, P>{
  StreamController<T> _valueController;
  List<StreamSubscription<T>> _valueSubscribers = [];
  T _value;
  bool get valueDone => _value != null;
  T get value => _value;
  bool _asyncStart;
  Future<T> Function(P) _asyncFunc;
  P _param;

  ValueController(P param, Future<T> Function(P) asyncFunc) {
    _param = param;
    _asyncStart = false;
    _asyncFunc = asyncFunc;
    _valueController = StreamController.broadcast();
  }

  void restart({P param}) {
    if (param != null) {
      _param = param;
    }
    stop();
    start();
  }

  void start() {
    if (!_asyncStart) {
      _asyncStart = true;
      _asyncFunc(_param).then((onValue) {
        _value = onValue;
        _valueController.sink.add(_value);
      });
    }
  }

  void stop() {
    _asyncStart = false;
    for (var c in _valueSubscribers) {
      c.cancel();
    }
    _value = null;
    // Метод cancel() контроллера вызывать не надо. Если асинхронная
    // функция попытается записать овет в поток закрытого контроллера,
    // то возникнет ошибка. Если контроллер будет переинициализирован,
    // то ничего плохого не произойдёт.
    _valueController = StreamController.broadcast();
  }

  void subscribe(void Function(T) func) {
    var subscriber = _valueController.stream.listen((onData) { func(onData); });
    _valueSubscribers.add(subscriber);
  }
}