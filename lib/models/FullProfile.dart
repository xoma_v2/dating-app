import 'dart:math' as math;

class FullProfile {
  List<String> photos;
  bool isFavorite;
  int id;
  int userId;
  String email;
  String phone;
  String name;
  String surname;
  String sex;
  String birthday;
  int ageFrom;
  int ageTo;
  Location location;
  List<int> answers;

  FullProfile(
      {this.photos,
      this.isFavorite,
      this.id,
      this.userId,
      this.email,
      this.phone,
      this.name,
      this.surname,
      this.sex,
      this.birthday,
      this.ageFrom,
      this.ageTo,
      this.location,
      this.answers});

  FullProfile.fromJson(Map<String, dynamic> json) {
    photos = json['photos'] != null
        ? json['photos'].cast<String>()
        : [];
    isFavorite = json['isFavorite'];
    id = json['id'];
    userId = json['userId'];
    email = json['email'];
    phone = json['phone'];
    name = json['name'];
    surname = json['surname'];
    sex = json['sex'];
    birthday = json['birthday'];
    ageFrom = json['ageFrom'];
    ageTo = json['ageTo'];
    location = json['location'] != null
        ? new Location.fromJson(json['location'])
        : null;
  }

  static List<FullProfile> fromJsonList(jsonList) {
    return jsonList.map<FullProfile>((obj) => FullProfile.fromJson(obj)).toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['photos'] = this.photos;
    data['isFavorite'] = this.isFavorite == null
        ? false
        : true;
    data['user'] = null;
    data['id'] = -1;
    data['userId'] = this.userId;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['name'] = this.name;
    data['surname'] = this.surname;
    data['sex'] = this.sex;
    data['birthday'] = this.birthday;
    data['ageFrom'] = this.ageFrom;
    data['ageTo'] = this.ageTo;
    if (this.location != null) {
      data['location'] = this.location.toJson();
    }
    return data;
  }
}

class Location {
  double latitude;
  double longitude;

  Location({this.latitude, this.longitude});

  Location.fromJson(Map<String, dynamic> json) {
    latitude = json['latitude'];
    longitude = json['longitude'];
  }

  double getDistanceTo(Location other) {
    double radiansOverDegrees = (math.pi / 180.0);

    double d1 = latitude * radiansOverDegrees;
    double num1 = longitude * radiansOverDegrees;
    double d2 = other.latitude * radiansOverDegrees;
    double num2 = other.longitude * radiansOverDegrees - num1;
    double d3 = math.pow(math.sin((d2 - d1) / 2.0), 2.0) +
                math.cos(d1) * math.cos(d2) * math.pow(math.sin(num2 / 2.0), 2.0);

    return 6376500.0 * (2.0 * math.atan2(math.sqrt(d3), math.sqrt(1.0 - d3)));
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    return data;
  }
}