import 'package:flutter/material.dart';
import 'package:dating_app/models/TestData.dart';
import 'package:dating_app/WebAPI.dart' as webAPI;
import 'package:dating_app/Globals.dart' as globals;

class TestScreen extends StatefulWidget {
  final Sex sex;
  final int userId;
  List<int> answers;
  
  TestScreen(this.sex, this.userId, {this.answers});

  @override
  _TestScreenState createState() => _TestScreenState(this.sex, this.userId, answers: this.answers);
}

class _TestScreenState extends State<TestScreen> {
  static final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  TestData _data;
  final int _userId;

  _TestScreenState(Sex sex, this._userId, {List<int> answers}) {
    _data = TestData(sex);
    if (answers != null) {
      _data.answers = answers;
    }
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        actions: <Widget>[
          if (widget.answers != null) IconButton(
            icon: Icon(Icons.save),
            onPressed: () {
              _scaffoldKey.currentState.removeCurrentSnackBar();
              if (_data.answers.contains(-1)) {
                _scaffoldKey.currentState.showSnackBar(const SnackBar(
                  content: Text('Не на все вопросы дан ответ'),
                ));
              } else {
                webAPI.putAnswers(_data.answers, _userId);
                globals.userProfile.answers = _data.answers;
                _scaffoldKey.currentState.showSnackBar(const SnackBar(
                  content: Text('Данные опроса сохранены'),
                ));
              }
            },
          ),
          if (widget.answers == null) IconButton(
            icon: Icon(Icons.arrow_forward),
            onPressed: () {
              if (_data.answers.contains(-1)) {
                _scaffoldKey.currentState.removeCurrentSnackBar();
                _scaffoldKey.currentState.showSnackBar(const SnackBar(
                  content: Text('Не на все вопросы дан ответ'),
                ));
              } else {
                webAPI.postAnswers(_data.answers, _userId);
                globals.userProfile.answers = _data.answers;
                Navigator.pushReplacementNamed(context, '/home');
              }
            },
          )
        ],
        title: Text("Опросник"),
        elevation: 0.0,
      ),
      body: questionList(),
    );
  }

  ListView questionList() {
    return ListView.builder(
      itemCount: _data.questions.length,
      itemBuilder: (context, index) => Card(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(10.0),
              child: Text(
              _data.questions[index],
              style: TextStyle(fontSize: 18.0),
              ),
            ),
            ListTile(
              dense: true,
              leading: Radio(
                value: 3,
                groupValue: _data.answers[index],
                onChanged: (value) => setState(() => _data.answers[index] = value),
              ),
              title: Text("Полностью согласен"),
              onTap: () => setState(() => _data.answers[index] = 3),
            ),
            ListTile(
              dense: true,
              leading: Radio(
                value: 2,
                groupValue: _data.answers[index],
                onChanged: (value) => setState(() => _data.answers[index] = value),
              ),
              title: Text("В общем, это верно"),
              onTap: () => setState(() => _data.answers[index] = 2),
            ),
            ListTile(
              dense: true,
              leading: Radio(
                value: 1,
                groupValue: _data.answers[index],
                onChanged: (value) => setState(() => _data.answers[index] = value),
              ),
              title: Text("Это не совсем так"),
              onTap: () => setState(() => _data.answers[index] = 1),
            ),
            ListTile(
              dense: true,
              leading: Radio(
                value: 0,
                groupValue: _data.answers[index],
                onChanged: (value) => setState(() => _data.answers[index] = value),
              ),
              title: Text("Это неверно"),
              onTap: () => setState(() => _data.answers[index] = 0),
            ),
          ],
        ),
      ),
    );
  }
}

