import 'package:dating_app/models/FullProfile.dart';
import 'package:dating_app/models/TestData.dart';
import 'package:flutter/material.dart';
import 'package:dating_app/Globals.dart' as globals;
import 'package:dating_app/WebAPI.dart' as webAPI;

enum OpenType {
  registration,
  editing,
}

class ProfileScreen extends StatefulWidget {
  final FullProfile profile;

  ProfileScreen({this.profile});

  @override
  _ProfileScreenState createState() => _ProfileScreenState(profile: profile);
}

class _ProfileScreenState extends State<ProfileScreen> {
  final _formKey = GlobalKey<FormState>();

  final _emailController = TextEditingController();
  final _phoneController = TextEditingController();
  final _nameController = TextEditingController();
  final _surnameController = TextEditingController();
  final _birthdayController = TextEditingController();
  final _ageFromController = TextEditingController();
  final _ageToController = TextEditingController();
  final _sexController = TextEditingController();
  //String _sex;
  Location _location;
  FullProfile _profile;

  _ProfileScreenState({FullProfile profile}) {
    _profile = profile;
    _location = Location(
      latitude: 55.771343,
      longitude: 37.691666,
    );
    if (profile != null) {
      _emailController.text = profile.email;
      _phoneController.text = profile.phone;
      _nameController.text = profile.name;
      _surnameController.text = profile.surname;
      _birthdayController.text = profile.birthday.substring(0, 10);
      _ageFromController.text = profile.ageFrom.toString();
      _ageToController.text = profile.ageTo.toString();
      _sexController.text = profile.sex == 'm'
          ? 'Мужской'
          : 'Женский';
      //_sex = profile.sex;
      _location = profile.location;
    }
  }

  @override
  void dispose() {
    _emailController.dispose();
    _phoneController.dispose();
    _nameController.dispose();
    _surnameController.dispose();
    _birthdayController.dispose();
    _ageFromController.dispose();
    _ageToController.dispose();
    super.dispose();
  }
 
  Future<Sex> _asyncSexDialog(BuildContext context) async {
    return await showDialog<Sex>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: const Text('Выберите ваш биологический пол '),
          children: <Widget>[
            SimpleDialogOption(
              onPressed: () => Navigator.pop(context, Sex.male),
              child: const Text('Мужской'),
            ),
            SimpleDialogOption(
              onPressed: () => Navigator.pop(context, Sex.female),
              child: const Text('Женский'),
            ),
          ],
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('${_profile == null ? 'Создайте' : 'Измените'} профиль'),
        actions: <Widget>[
          if (_profile != null) IconButton(
            icon: Icon(Icons.save),
            onPressed: () {},
          ),
          if (_profile == null) IconButton(
            icon: Icon(Icons.arrow_forward),
            onPressed: () {
              if (_formKey.currentState.validate()) {
                List<String> b = _birthdayController.text.split('-');
                FullProfile profile = FullProfile(
                  userId: globals.userId,
                  location: _location,
                  name: _nameController.text,
                  surname: _surnameController.text,
                  sex: _sexController == 'Мужской'
                      ? 'm'
                      : 'w',
                  phone: _phoneController.text,
                  email: _emailController.text,
                  birthday: '${b[0]}-${b[1].length == 1 ? '0' + b[1] : b[1]}-${b[2].length == 1 ? '0' + b[2] : b[2]}T00:00:00',
                );

                globals.userProfile = profile;

                webAPI.putFullProfile(profile).then((value) {
                  Navigator.pushNamed(context, '/profile/test');
                });
              }
            },
          )
        ],
      ),
      body: Form(
        key: _formKey,
        child: Center(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 24.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: 12.0),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text("ОБЯЗАТЕЛЬНАЯ ИНФОРМАЦИЯ"),
                  ),
                  SizedBox(height: 12.0),
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: "Имя",
                      labelStyle: TextStyle(color: Colors.blue),
                      hintText: "Введите ваше имя",
                      filled: true,
                      errorMaxLines: 3,
                    ),
                    controller: _nameController,
                    validator: (value) {
                      if (RegExp(r'^[А-Я]{1}[а-я]+$').hasMatch(value)) return null;
                      return 'Первая буква должна быть заглавной, а остальные строчными. Допускаются только русские буквы';
                    },
                  ),
                  SizedBox(height: 12.0),
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: "Фамилия",
                      labelStyle: TextStyle(color: Colors.blue),
                      hintText: "Введите вашу фамилию",
                      filled: true,
                      errorMaxLines: 3,
                    ),
                    controller: _surnameController,
                    validator: (value) {
                      if (RegExp(r'^[А-Я]{1}[а-я]+$').hasMatch(value)) return null;
                      return 'Первая буква должна быть заглавной, а остальные строчными. Допускаются только русские буквы';
                    },
                  ),
                  SizedBox(height: 12.0),
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: "Дата рождения",
                      labelStyle: TextStyle(color: Colors.blue),
                      hintText: "Допустимый формат ГГГГ-ММ-ДД",
                      filled: true,
                      errorMaxLines: 3,
                    ),
                    controller: _birthdayController,
                    keyboardType: TextInputType.datetime,
                    validator: (value) {
                      if (RegExp(r'^([12]\d{3}-([1-9]|0[1-9]|1[0-2])-([1-9]|0[1-9]|[12]\d|3[01]))$').hasMatch(value)) return null;
                      return 'Введите дату рождения в формате ГГГГ-ММ-ДД';
                    },
                  ),
                  SizedBox(height: 12.0),
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: "Биологический пол",
                      labelStyle: TextStyle(color: Colors.blue),
                      filled: true,
                      errorMaxLines: 3,
                    ),
                    controller: _sexController,
                    onTap: () async {
                      final Sex sex = await _asyncSexDialog(context);
                      _sexController.text = sex == Sex.male
                          ? 'Мужской'
                          : 'Женский';
                    },
                    validator: (value) {
                      if (value.isEmpty) return 'Не выбран пол';
                      return null;
                    },
                  ),
                  SizedBox(height: 12.0),
                  if (_profile != null) Container(
                    width: double.infinity,
                    child: RaisedButton(
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Изменить опросник',
                          textAlign: TextAlign.left,
                          style: TextStyle(color: Colors.blue)
                        ),
                      ),
                      onPressed: () { Navigator.pushNamed(context, '/home/profile/test'); },
                      color: Colors.grey[200],
                    ),
                  ),
                  if (_profile != null) SizedBox(height: 12.0),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text("НЕОБЯЗАТЕЛЬНАЯ ИНФОРМАЦИЯ"),
                  ),
                  SizedBox(height: 12.0),
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: "Электронная почта",
                      labelStyle: TextStyle(color: Colors.blue),
                      hintText: "Введите ваш уникальный адрес электронной почты",
                      filled: true,
                      errorMaxLines: 3,
                    ),
                    controller: _emailController,
                    keyboardType: TextInputType.emailAddress,
                    validator: (value) {
                      if (value.isEmpty) return null;
                      if (RegExp(r'^(.+@.+\..+)$').hasMatch(value)) return null;
                      return 'Некорректный адрес электронной почты';
                    },
                  ),
                  SizedBox(height: 12.0),
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: "Номер телефона",
                      labelStyle: TextStyle(color: Colors.blue),
                      hintText: "Введите российский номер мобильного телефона",
                      filled: true,
                      errorMaxLines: 3,
                    ),
                    controller: _phoneController,
                    keyboardType: TextInputType.phone,
                    validator: (value) {
                      if (value.isEmpty) return null;
                      if (RegExp(r'^((\+7|7|8){1}([0-9]){10})$').hasMatch(value)) return null;
                      return 'Доступны следующие регионы: +7, 7, 8. Остальная часть номера может состоять только из цифр';
                    },
                  ),
                  SizedBox(height: 12.0),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}