import 'package:flutter/material.dart';
import 'package:dating_app/screens/DatingPage.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DefaultTabController(
        length: 3,
        initialIndex: 0,
        child: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) => [
            SliverAppBar(
              title: Text('Tindor'),
              floating: true,
              pinned: false,
              snap: true,
              actions: [
                IconButton(
                  icon: Icon(Icons.person),
                  onPressed: () {
                    Navigator.pushNamed(context, '/home/profile');
                  },
                ),
                IconButton(
                  icon: Icon(Icons.exit_to_app),
                  onPressed: () {
                    Navigator.pushReplacementNamed(context, '/');
                  },
                )
              ],
              bottom: TabBar(
                tabs: [
                  Tab(
                    icon: Icon(Icons.location_on),
                    text: 'Ближайшие',
                  ),
                  Tab(
                    icon: Icon(Icons.star),
                    text: 'Подобранные',
                  ),
                  Tab(
                    icon: Icon(Icons.favorite),
                    text: 'Понравившиеся',
                  ),
                ],
                labelColor: Theme.of(context).accentColor,
                unselectedLabelColor: Theme.of(context).unselectedWidgetColor,
              ),
            )
          ],
          body: TabBarView(
            children: <Widget>[
              DatingPage(DatingType.closest),
              DatingPage(DatingType.marriage),
              DatingPage(DatingType.favorite),
            ],
          ),
        ),
      ),
    );
  }
}
