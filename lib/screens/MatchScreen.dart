import 'dart:convert';
import 'package:dating_app/models/FullProfile.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:carousel_slider/carousel_slider.dart';

class _ContactCategory extends StatelessWidget {
  const _ContactCategory({ Key key, this.icon, this.children }) : super(key: key);

  final IconData icon;
  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      decoration: BoxDecoration(
        border: Border(bottom: BorderSide(color: themeData.dividerColor))
      ),
      child: DefaultTextStyle(
        style: Theme.of(context).textTheme.subhead,
        child: SafeArea(
          top: false,
          bottom: false,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: const EdgeInsets.symmetric(vertical: 24.0),
                width: 72.0,
                child: Icon(icon, color: themeData.accentColor),
              ),
              Expanded(child: Column(children: children)),
            ],
          ),
        ),
      ),
    );
  }
}

class _ContactItem extends StatelessWidget {
  const _ContactItem({ Key key, this.icon, this.lines, this.tooltip, this.onPressed })
    : assert(lines.length > 1),
      super(key: key);

  final IconData icon;
  final List<String> lines;
  final String tooltip;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    final List<Widget> columnChildren = lines.sublist(0, lines.length - 1).map<Widget>((String line) => Text(line)).toList();
    columnChildren.add(Text(lines.last, style: themeData.textTheme.caption));

    final List<Widget> rowChildren = <Widget>[
      Expanded(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: columnChildren,
        ),
      ),
    ];
    if (icon != null) {
      rowChildren.add(SizedBox(
        width: 72.0,
        child: IconButton(
          icon: Icon(icon),
          color: themeData.accentColor,
          onPressed: onPressed,
        ),
      ));
    }
    return MergeSemantics(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: rowChildren,
        ),
      ),
    );
  }
}

class MatchScreen extends StatefulWidget {
  FullProfile profile;

  MatchScreen(this.profile);

  @override
  _MatchScreenState createState() => new _MatchScreenState();
}

class _MatchScreenState extends State<MatchScreen> {
  static final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Text('${widget.profile.name} ${widget.profile.surname}'),
      ),
      body: Form(
        child: Scrollbar(
          child: ListView(
            padding: const EdgeInsets.all(16.0),
            children: <Widget>[
              if (widget.profile.photos != null && widget.profile.photos.length != 0)
                CarouselSlider(
                  items: widget.profile.photos.map((i) => Builder(
                    builder: (BuildContext context) => Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.symmetric(horizontal: 5.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                        child: Image.memory(
                          base64Decode(i),
                          fit: BoxFit.fitHeight,
                        ),
                      ),
                    ),
                  )).toList(),
                  aspectRatio: 1/1,
                  enableInfiniteScroll: false,
                  enlargeCenterPage: true,
                ),
              if (widget.profile.phone != null) _ContactCategory(
                icon: Icons.call,
                children: <Widget>[
                  _ContactItem(
                    icon: Icons.content_copy,
                    onPressed: () {
                      Clipboard.setData(ClipboardData(text: widget.profile.phone));
                      _scaffoldKey.currentState.showSnackBar(const SnackBar(
                        content: Text('Номер скопирован в буфер обмена'),
                      ));
                    },
                    lines: <String>[
                      widget.profile.phone,
                      'Мобильный телефон',
                    ]
                  ),
                ],
              ),
              if (widget.profile.email != null) _ContactCategory(
                icon: Icons.email,
                children: <Widget>[
                  _ContactItem(
                    icon: Icons.content_copy,
                    onPressed: () {
                      Clipboard.setData(ClipboardData(text: widget.profile.email));
                      _scaffoldKey.currentState.showSnackBar(const SnackBar(
                        content: Text('Адрес электронной почты скопирован в буфер обмена'),
                      ));
                    },
                    lines: <String>[
                      widget.profile.email,
                      'Электронная почта',
                    ],
                  ),
                ],
              ),
              _ContactCategory(
                icon: Icons.date_range,
                children: <Widget>[
                  _ContactItem(
                    lines: <String>[
                      widget.profile.birthday.substring(0, 10),
                      'День рождения',
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}