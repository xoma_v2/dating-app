import 'package:dating_app/widgets/DatingGrid.dart';
import 'package:flutter/material.dart';
import 'package:dating_app/Globals.dart' as globals;

enum DatingType {
  closest,
  marriage,
  favorite,
}

class DatingPage extends StatefulWidget {
  final DatingType datingType;

  DatingPage(this.datingType);

  @override
  _DatingPageState createState() => _DatingPageState();
}

class _DatingPageState extends State<DatingPage> with AutomaticKeepAliveClientMixin<DatingPage> {
  List<int> userIds;
  
  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    
    if (widget.datingType == DatingType.closest) {
      globals.closestController.onReady(_onDone);
    } else if (widget.datingType == DatingType.marriage) {
      globals.marriageController.onReady(_onDone);
    } else {
      globals.favoriteController.onReady(_onDone);
    }
  }

  _onDone(List<int> value) {
    userIds = value;
    if (this.mounted) { setState(() {}); }
  }

  @override
  Widget build(BuildContext context) {
    if (userIds == null) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Получение информации о ${widget.datingType == DatingType.closest ? 'ближайших' : widget.datingType == DatingType.marriage ? 'наиболее подходящих' : 'понравившихся'} партнёрах...',
            style: TextStyle(fontSize: 15.0),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 15.0),
          CircularProgressIndicator(),
        ],
      );
    } else {
      return PagewiseDatingGrid(userIds);
    }
  }
}
