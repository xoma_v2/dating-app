import 'package:dating_app/models/FullProfile.dart';
import 'package:dating_app/models/ValueController.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:dating_app/WebAPI.dart' as webAPI;
import 'package:dating_app/Globals.dart' as globals;

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  static final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  final _loginController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  void dispose() {
    _loginController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 24.0),
              child: Column(
                children: <Widget>[
                  SizedBox(height: 12.0),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text("ВОЙДИТЕ В СВОЙ АККАУНТ"),
                  ),
                  SizedBox(height: 12.0),
                  TextField(
                    decoration: InputDecoration(
                      labelText: "Имя пользователя",
                      labelStyle: TextStyle(color: Colors.blue),
                      hintText: "Введите логин",
                      filled: true,
                    ),
                    controller: _loginController,
                  ),
                  SizedBox(height: 12.0),
                  TextField(
                    decoration: InputDecoration(
                      labelText: "Пароль",
                      labelStyle: TextStyle(color: Colors.blue),
                      hintText: "Введите пароль",
                      filled: true,
                    ),
                    obscureText: true,
                    controller: _passwordController,
                  ),
                  SizedBox(height: 12.0),
                  Row(
                    children: <Widget>[
                      FlatButton(
                        child: Text("ЗАРЕГИСТРИРОВАТЬСЯ"),
                        onPressed: () => Navigator.pushNamed(context, '/registration'),
                      ),
                      Spacer(),
                      RaisedButton(
                        child: Text("ВОЙТИ"),
                        onPressed: () {
                          _scaffoldKey.currentState.removeCurrentSnackBar();
                          webAPI.authUser(_loginController.text, _passwordController.text).then((responseCode) {
                            if (responseCode == 200) {
                              webAPI.getFullProfile(globals.userId).then((profile) {
                                if (profile == null) {
                                  Navigator.pushReplacementNamed(context, '/profile');
                                } else {
                                  globals.userProfile = profile;
                                  webAPI.getAnswers(globals.userId).then((answers) {
                                    if (answers == null) {
                                      Navigator.pushReplacementNamed(context, '/profile/test');
                                    } else {
                                      globals.userProfile.answers = answers;
                                      Navigator.pushReplacementNamed(context, '/home');
                                    }
                                  });
                                }
                              });
                            } else {
                              _scaffoldKey.currentState.showSnackBar(const SnackBar(
                                content: Text('Аутентификация не пройдена'),
                              ));
                            }
                          });
                        },
                      ),
                    ],
                  ),
                  SizedBox(height: 12.0),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}