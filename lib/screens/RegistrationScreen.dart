import 'package:flutter/material.dart';
import 'package:dating_app/WebAPI.dart' as webAPI;

class RegistrationScreen extends StatefulWidget {
  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  static final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();

  final _loginController = TextEditingController();
  final _passwordController = TextEditingController();
  final _passwordCheckController = TextEditingController();

  @override
  void dispose() {
    _loginController.dispose();
    _passwordController.dispose();
    _passwordCheckController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Создайте аккаунт'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.arrow_forward),
            onPressed: () {
              if (_formKey.currentState.validate()) {
                _scaffoldKey.currentState.removeCurrentSnackBar();
                webAPI.putUser(_loginController.text, _passwordController.text).then((value) {
                  if (value == true) {
                    Navigator.pop(context);
                  } else {
                    _scaffoldKey.currentState.showSnackBar(const SnackBar(
                      content: Text('Данный логин уже занят, попробуйте другой'),
                    ));
                  }
                });
              }
            },
          ),
        ],
      ),
      body: Form(
        key: _formKey,
        child: Center(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 24.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: 12.0),
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: "Имя пользователя",
                      labelStyle: TextStyle(color: Colors.blue),
                      hintText: "Введите логин для входа в систему",
                      filled: true,
                      errorMaxLines: 3,
                    ),
                    controller: _loginController,
                    validator: (value) {
                      if (RegExp(r'^[a-zA-Z0-9_\-!]{5,20}$').hasMatch(value)) return null;
                      return 'Логин должен содержать от 5 до 20 символов, среди которых буквы латинского алфавита, цифры, "!", "_", "-"';
                    },
                  ),
                  SizedBox(height: 12.0),
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: "Пароль",
                      labelStyle: TextStyle(color: Colors.blue),
                      hintText: "Введите пароль",
                      filled: true,
                      errorMaxLines: 3,
                    ),
                    obscureText: true,
                    controller: _passwordController,
                    validator: (value) {
                      if (RegExp(r'^[a-zA-Z0-9_\-!]{5,20}$').hasMatch(value)) return null;
                      return 'Пароль должен содержать от 5 до 20 символов, среди которых буквы латинского алфавита, цифры, "!", "_", "-"';
                    },
                  ),
                  SizedBox(height: 12.0),
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: "Повтор пароля",
                      labelStyle: TextStyle(color: Colors.blue),
                      hintText: "Введите пароль повторно",
                      filled: true,
                      errorMaxLines: 3,
                    ),
                    obscureText: true,
                    controller: _passwordCheckController,
                    validator: (value) {
                      if (value.isEmpty) return 'Заполните поле';
                      if (_passwordController.text == _passwordCheckController.text) return null;
                      return 'Пароли не совпадают';
                    },
                  ),
                  SizedBox(height: 12.0),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}