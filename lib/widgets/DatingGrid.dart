import 'dart:convert';
import 'package:dating_app/Globals.dart' as globals;
import 'package:dating_app/models/FullProfile.dart';
import 'package:dating_app/screens/MatchScreen.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:dating_app/WebAPI.dart' as webAPI;
import 'package:flutter_pagewise/flutter_pagewise.dart';

typedef ItemTapCallback = void Function(FullProfile profile);

class _GridTitleText extends StatelessWidget {
  final String text;

  const _GridTitleText(this.text);

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      alignment: Alignment.centerLeft,
      child: Text(text),
    );
  }
}

class _MyIconButton extends StatefulWidget {
  FullProfile profile;

  _MyIconButton(this.profile);

  @override
  _MyIconButtonState createState() => new _MyIconButtonState();
}

class _MyIconButtonState extends State<_MyIconButton> {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(widget.profile.isFavorite ? Icons.favorite : Icons.favorite_border),
      color: widget.profile.isFavorite ? Colors.red : Colors.white,
      onPressed: () => setState(() {
        if (widget.profile.isFavorite) {
          webAPI.deleteFavorite(widget.profile.userId, globals.userId);
        } else {
          webAPI.putFavorite(widget.profile.userId, globals.userId);
        }

        widget.profile.isFavorite = !widget.profile.isFavorite;
      }),
    );
  }
}

class GridPhotoItem extends StatelessWidget {
  final FullProfile profile;
  ItemTapCallback onItemTap = (profile) {};

  GridPhotoItem({
    Key key,
    /*@required */this.profile,
    @required this.onItemTap,
  }) : assert(profile != null),
       //assert(onItemTap != null),
       super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onItemTap(profile),
      child: GridTile(
        child: Image.memory(
          //'${globals.domain}api/datingapp/photo/${String.fromCharCodes(base64Decode(profiles[0].photos.first))}',
          base64Decode((profile.photos == null || profile.photos.length == 0) ? globals.noPhoto : profile.photos.first),
          fit: BoxFit.cover,
        ),
        footer: GridTileBar(
          backgroundColor: Colors.black26,
          title: _GridTitleText('${profile.name} ${profile.surname}'),
          subtitle: _GridTitleText('В ${(profile.location.getDistanceTo(globals.userProfile.location) / 1000).round()} км от Вас'),
          trailing: _MyIconButton(profile),
        ),
      ),
    );
  }
}

class PagewiseDatingGrid extends StatelessWidget {
  static const int PAGE_SIZE = 12;
  final List<int> userIds;
  
  PagewiseDatingGrid(this.userIds);

  @override
  Widget build(BuildContext context) {
    final Orientation orientation = MediaQuery.of(context).orientation;

    return PagewiseGridView.count(
      pageSize: PAGE_SIZE,
      crossAxisCount: orientation == Orientation.portrait ? 2 : 3,
      mainAxisSpacing: 2.0,
      crossAxisSpacing: 2.0,
      childAspectRatio: orientation == Orientation.portrait ? 1.0 : 1.3,
      //padding: EdgeInsets.all(2.0),
      itemBuilder: (context, FullProfile entry, _) => GridPhotoItem(
        profile: entry,
        onItemTap: (FullProfile profile) {
          Navigator.push(context, MaterialPageRoute(
            builder: (context) => MatchScreen(profile),
            fullscreenDialog: true,
          ));
          //Navigator.pushNamed(context, '/home/profile', arguments: profile);
        },
      ),
      pageFuture: (pageIndex) {
        if ((pageIndex + 1) * PAGE_SIZE <= userIds.length) {
          return webAPI.getFullProfiles(userIds.sublist(pageIndex * PAGE_SIZE, (pageIndex + 1) * PAGE_SIZE), globals.userId);
        } else if (pageIndex * PAGE_SIZE < userIds.length)  {
          return webAPI.getFullProfiles(userIds.sublist(pageIndex * PAGE_SIZE), globals.userId);
        } else {
          throw Error();
        }
      },
    );
  }
}